skfdiff.core.backends package
=============================

Submodules
----------

skfdiff.core.backends.base\_backend module
------------------------------------------

.. automodule:: skfdiff.core.backends.base_backend
    :members:
    :undoc-members:
    :show-inheritance:

skfdiff.core.backends.numba\_backend module
-------------------------------------------

.. automodule:: skfdiff.core.backends.numba_backend
    :members:
    :undoc-members:
    :show-inheritance:

skfdiff.core.backends.numpy\_backend module
-------------------------------------------

.. automodule:: skfdiff.core.backends.numpy_backend
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: skfdiff.core.backends
    :members:
    :undoc-members:
    :show-inheritance:
