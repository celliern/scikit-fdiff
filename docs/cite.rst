How to cite
===========

Scikit-fdiff can be cited in two different DOI, with different purpose.

The first one is generated via `Zenodo <https://zenodo.org/>`_ and is linked to
a specific release. This DOI is useful to ensure the repeatability of your
research. An archive of that release is available for anyone using that DOI,
which guarantee that they will use the same tool as you, which should lead to
the same result.

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3236970.svg
   :target: https://doi.org/10.5281/zenodo.3236970

The second one is a `JOSS <https://joss.theoj.org/>`_ publication that is
linked to the repository. This is useful to cite scikit-fdiff as software,
and is a kind way to thank the author of that library and to recognise the
usefulness of their works.

.. image:: http://joss.theoj.org/papers/5e93e199cb128c2f833d04ffea4bdf86/status.svg
   :target: http://joss.theoj.org/papers/5e93e199cb128c2f833d04ffea4bdf86

Both of them are available in the main page of the repository.
