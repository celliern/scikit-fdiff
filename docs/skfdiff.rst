skfdiff package
===============

scikit-fdiff model
------------------

.. automodule:: skfdiff.model
    :members:
    :undoc-members:
    :show-inheritance:

scikit-fdiff simulation
-----------------------

.. automodule:: skfdiff.simulation
    :members:
    :undoc-members:
    :show-inheritance:

scikit-fdiff temporal schemes
-----------------------------

.. automodule:: skfdiff.core.temporal_schemes
    :members:
    :undoc-members:
    :show-inheritance:
