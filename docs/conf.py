from path import Path
from sphinx_gallery.scrapers import figure_rst


project = "Scikit FiniteDiff"
copyright = "2019, Nicolas Cellier"
author = "Nicolas Cellier"

version = "0.6"
release = "0.6.0a1"

extensions = [
    "nb2plots",
    "IPython.sphinxext.ipython_directive",
    "IPython.sphinxext.ipython_console_highlighting",
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx_gallery.gen_gallery",
]

sphinx_gallery_conf = {
    "examples_dirs": "../examples",  # path to your example scripts
    "gallery_dirs": "auto_examples",  # path where to save gallery generated examples
    "image_scrapers": ("matplotlib",),
    "min_reported_time": 3600,
}

napoleon_numpy_docstring = True

templates_path = ["_templates"]
source_suffix = ".rst"

master_doc = "index"
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
pygments_style = None

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]


def setup(app):
    app.add_css_file("custom.css")


htmlhelp_basename = "ScikitFiniteDiffdoc"

latex_documents = [
    (
        master_doc,
        "ScikitFiniteDiff.tex",
        "Scikit FiniteDiff Documentation",
        "Nicolas Cellier",
        "manual",
    )
]

man_pages = [
    (master_doc, "scikitfinitediff", "Scikit FiniteDiff Documentation", [author], 1)
]

texinfo_documents = [
    (
        master_doc,
        "ScikitFiniteDiff",
        "Scikit FiniteDiff Documentation",
        author,
        "ScikitFiniteDiff",
        "One line description of project.",
        "Miscellaneous",
    )
]
epub_title = project

epub_exclude_files = ["search.html"]
